﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameMode
{
    PlayAgain,
    SinglePlayer,
    MultiPlayer,
}

public class GameParams : MonoBehaviour
{
    public static GameParams Instance {
        get
        {
            if(instance == null)
            {
                instance = Instantiate(Resources.Load<GameParams>("Prefabs/Game"));
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
        
    }

    private static GameParams instance;

    public GameMode GameMode { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
