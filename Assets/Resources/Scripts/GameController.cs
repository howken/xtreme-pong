﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    private int scorePlayer = 0;
    private int scoreOpponent = 0;

    public GameObject scoreTextPlayer;
    public GameObject scoreTextOpponent;

    public GameObject fieldPrefab;
    private GameObject field;

    public int goalToWin;

    public void goalPlayer()
    {
        this.scorePlayer++;
    }

    public void goalOpponent()
    {
        this.scoreOpponent++;
    }

    private void Start()
    {
        Instance = this;

        if (this.field)
        {
            Destroy(field);
        }
        this.field = Instantiate(fieldPrefab);
    }

    private void FixedUpdate()
    {
        // update UI
        Text uiScorePlayer = this.scoreTextPlayer.GetComponent<Text>();
        uiScorePlayer.text = this.scorePlayer.ToString();

        Text uiScoreOpponent = this.scoreTextOpponent.GetComponent<Text>();
        uiScoreOpponent.text = this.scoreOpponent.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if(this.scorePlayer >= this.goalToWin || this.scoreOpponent >= this.goalToWin)
        {
            Debug.Log("Game Over");
            SceneManager.LoadScene("GameOver");
        }
    }
}
