﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMainMenuButton : MonoBehaviour
{
    public void BackToMainMenu()
    {
        Debug.Log("Back To Main Menu Pressed");

        SceneManager.LoadScene("MainMenu");
    }
}
