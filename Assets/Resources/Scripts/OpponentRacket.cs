﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentRacket : MonoBehaviour
{

    public float movementSpeed;

    private void FixedUpdate()
    {
        if(GameParams.Instance.GameMode == GameMode.MultiPlayer)
        {
            float v = Input.GetAxisRaw("Vertical2");

            GetComponent<Rigidbody2D>().velocity = new Vector2(0, v) * this.movementSpeed;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
