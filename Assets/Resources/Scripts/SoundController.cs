﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public static SoundController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Instantiate(Resources.Load<SoundController>("Prefabs/Sound"));
            }
            return instance;
        }
        private set
        {
            instance = value;
        }

    }
    private static SoundController instance;

    public AudioClip wallSound;
    public AudioClip racketSound;

    public AudioSource musicSource;
    public AudioSource sfxSource;

    private void Start()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void PlayRacketSound()
    {
        this.sfxSource.clip = racketSound;
        this.sfxSource.Play();
    }

    public void PlayWallSound()
    {
        this.sfxSource.clip = wallSound;
        this.sfxSource.Play();
    }

}
