﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    public float movementSpeed;
    public float bonusSpeedPerHit;
    public float maxBonusSpeed;

    private int hitCounter = 0;



    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start Ball in 2 Seconds");
        StartCoroutine(this.StartBall());
    }

    void PositionBall(bool isStartingPlayer)
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

        if (isStartingPlayer)
        {
            this.gameObject.transform.localPosition = new Vector3(-100, 0, 0);
        }
        else
        {
            this.gameObject.transform.localPosition = new Vector3(100, 0, 0);
        }
    }

    // coroutine
    public IEnumerator StartBall(bool isStartingPlayer = true)
    {
        this.PositionBall(isStartingPlayer);

        this.hitCounter = 0;
        yield return new WaitForSeconds(2);

        if(isStartingPlayer)
        {
            this.MoveBall(new Vector2(-1, 0));
        }
        else
        {
            this.MoveBall(new Vector2(1, 0));
        }
    }

    public void MoveBall(Vector2 dir)
    {
        dir = dir.normalized;

        float speed = this.movementSpeed + this.bonusSpeedPerHit * this.hitCounter;

        Rigidbody2D rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();

        rigidbody2D.velocity = dir * speed;
    }

    public void increaseHitCounter()
    {
        if(this.hitCounter * this.bonusSpeedPerHit <= this.maxBonusSpeed)
        {
            this.hitCounter++;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Racket"))
        {
            SoundController.Instance.PlayRacketSound();
            this.BounceFromRacket(collision);
        }
        else if (collision.collider.CompareTag("RegularWall"))
        {
            SoundController.Instance.PlayWallSound();
        }
        else if (collision.collider.CompareTag("PlayerGoal"))
        {
            Debug.Log("Collision with wall left");
            GameController.Instance.goalOpponent();
            StartCoroutine(this.StartBall(true));
        }
        else if (collision.collider.CompareTag("OpponentGoal"))
        {
            Debug.Log("Collision with wall right");
            GameController.Instance.goalPlayer();
            StartCoroutine(this.StartBall(false));
        }

    }

    void BounceFromRacket(Collision2D c)
    {
        Vector3 ballPosition = this.transform.position;
        Vector3 racketPosition = c.gameObject.transform.position;

        float racketHeight = c.collider.bounds.size.y;

        float x = 0;

        if (c.gameObject.GetComponent<PlayerRacket>() != null)
        {
            x = 1;
        }
        else if (c.gameObject.GetComponent<OpponentRacket>() != null)
        {
            x = -1;
        }

        float y = (ballPosition.y - racketPosition.y) / racketHeight;

        this.increaseHitCounter();
        this.MoveBall(new Vector2(x, y));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
