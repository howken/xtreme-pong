﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayButton : MonoBehaviour
{
    // Napravi Enum za game mode
    // ali za sad isMultiplayer bool
    public GameMode gameMode;

    public void PlayGame()
    {
        if (this.gameMode != GameMode.PlayAgain) GameParams.Instance.GameMode = this.gameMode;
        Debug.Log("Play Game Pressed - mode - " + GameParams.Instance.GameMode.ToString());

        SceneManager.LoadScene("Game");
    }
}
